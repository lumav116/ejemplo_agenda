create table "CURSO".CAR
(
                ID INTEGER not null primary key,
                NOMBRE VARCHAR(255),
                CORREO VARCHAR(255),
				TELEFONO VARCHAR(10),
                IS_CONTACTO VARCHAR(5),
                POSICION_IMAGEN NUMERIC
)