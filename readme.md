
# \<contacts-app\>

## Descripcion
##### Autor: LMAV - Luis Manuel Alducin Vazquez
##### Email: luis.alducin@softtek.com
##### Descripción: Proyecto final del curso de Polymer 2.0
##### Fecha: 4 de marzo 2019

## Dependencias
| Front End | Archivo | Descripcion |
| ---------- | ------- | ----------- |
| Front | contacts-app.html | Archivo de componente |
| Front | contact-card.html | Archivo de componente |
| Front | carrusel-element.html | Archivo de componente |
| Back | tabla.sql | BD Datos Derby, Crear la tabla contactos en el script tablas.sql |
| Back | proyecto netbeans | JSConsole |

## Instrucciones adicionales
N/A

## Armar proyecto
```
$ bower install
$ polymer serve --open
```

## Visualizar aplicacion
```
$ polymer serve --open
```
Abrir en [http://loalhost:8080/components/polymer-contacts]
